class UserPagesController < ApplicationController
  def regist
    @user = User.new
  end

  def profile
    @user = User.find(params[:id])
  end

  def edit
  end

  def login
  end

  def favdeleteselect
    @user = current_user
  end

  def favdelete
    @user = current_user
    favid = params[:fav]
    favid.each do |id|
      @favrecipe = Dish.find(id)
      @user.dishes.destroy(@favrecipe)
    end
    redirect_to :back
  end

  def destroyinput
    @user = current_user
  end

  def destroyconfirm
    @user = current_user
    if @user.authenticate(params[:user][:password])
    else
      flash[:notices] = "パスワードが間違っています"
      redirect_to :back
    end
  end

  def destroy
    @user = current_user
    @user.destroy
    redirect_to root_path
  end

  def allergies
    @all = Allergy.all
    @user = current_user
  end

  def allergiesinput
    alle = params[:alle]
    @allstr = ""
    if alle.nil?
      @allstr = nil
    else
      alle.each do |all|
        @allstr = @allstr + all.to_s
        if all != alle[-1]
          @allstr = @allstr + ","
        end
      end
    end
    user = current_user
    user.alvalue = @allstr
    user.save!
  end

  def namechange
    @user = current_user
  end

  def newemailinput
    @user = current_user
  end

  def emailchange
    @user = User.find(params[:id])
    @user.email = params[:newemail]
  end

  def sentnewmail
    @user = current_user
    @emailadd = user_params[:email]
    same = false
    @userall = User.all
    @userall.each do |userall|
      if @emailadd == userall.email
        same = true
      end
    end
    if @user.email == @emailadd || same
      redirect_to :back
      flash[:info] = "現在と同一のアドレスか、既に使用されているアドレスです"
    else
      @user.email = @emailadd
      @user.send_email_change_email
      redirect_to user_page_path(current_user)
      flash[:info] = "入力されたメールアドレスへメールアドレス再設定メールを送信しました。"
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      if logged_in?
        redirect_to user_page_path(current_user)
      else
        redirect_to root_path
      end
      flash[:notices] = "ユーザー情報を変更しました"
    else
      redirect_to :back
      flash[:notices] = "変更に失敗しました"
    end
  end

  def create
    @user = User.new(user_params)
    if params[:back]
      render 'regist'
    elsif @user.save
      @user.send_activation_email
      flash[:info] = "入力されたメールアドレスにメールを送信しました。"
      redirect_to root_url
    else
      render 'regist'
    end
  end

  def confirm
    @user = User.new(user_params)
    render 'regist' if @user.invalid?
  end

  private

  def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
  end
end

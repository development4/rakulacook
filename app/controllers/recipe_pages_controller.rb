class RecipePagesController < ApplicationController
  def result
    # @member    = params[:member][:people]
    # @side_dish = params[:side_dish][:item]
    # @main = Dish.find(params[:main])
    # @sub1 = Dish.find(params[:sub])
    @main = Dish.find(session[:maindish])
    @sub = Dish.find(session[:subdish])

    # @sub2 = Dish.find(params[:sub2])
  end

  def recipe
    # @member    = params[:member]
    @recipe = JSON.parse(Dish.find(params[:id]).recipe)
    @food = JSON.parse(Dish.find(params[:id]).food)
    @favcnt = Dish.find(params[:id])
    if logged_in?
      @user = current_user
    end
  end

  def test
    # 提案する献立に関する情報はsessionに入れて渡す
    # testではsessionにデータをベタ打ちしている。
    # 本当のデータはquestion_resultでsessionに書き込む
    #
    # session[:maindish] は主菜の料理IDを入れるセッション変数
    # array は複数の副菜の料理IDが入った配列
    # session[:subdish] は副菜の配列を入れるセッション変数
    # session[:people]は人数を入れるセッション変数
    @maintest = Dish.first
    session[:maindish] = @maintest.id

    @subtest = Dish.last(4)
    array = Array.new
    @subtest.each do |s|
      array.push(s.id)
    end

    session[:subdish] = array
    session[:people] = 2

    #session変数に値を入れたらViewは書き出さずにリダイレクトする
    redirect_to :action => "result"

  end

  def fav
    @recipe = Dish.find(params[:id])
    @user = current_user
    @user.dishes << @recipe
#    @recipe.users << @user
  end

end
